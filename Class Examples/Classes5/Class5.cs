using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{

			// Put code here to create 3 car instances
            AnotherCar car1 = new AnotherCar(); // Constructor Method
            car1.Make = "Forda";
            car1.Colour = "Blue";
            car1.Type= "Mondo";
            car1.Cost = 24500;
            
            
            Console.WriteLine(car1.Country + " " + car1.Make + " " + car1.Type + " " + car1.Colour + " " + car1.Cost);

            // Instance Car 2 

            AnotherCar car2 = new AnotherCar();
            car2.Make = "Toyota";
            car2.Colour = "Green";
            car2.Type = "Ferrari";
            car2.Cost = 235555;

            Console.WriteLine(car2.Make + " " + car2.Colour + " " + car2.Type + " " + car2.Cost);

            //Instance Car 3

            AnotherCar car3 = new AnotherCar();
            car3.Make = "Bugatti";
            car3.Colour = "Magenta";
            car3.Type = "Veyron";
            car3.Cost = 22334234;
             
            Console.WriteLine(car3.Make + " " + car3.Colour + " " + car3.Type + " " + car3.Cost);

            car1.Display();
            car2.Display();
            car3.Display();

            Console.ReadLine();
            // Instance Car 3
			// Add a method named 'Display'to the AnotherCar Class
			// This method should output the details of a  car instance
			// to the Console

			// Put code here to call that method to display the 
			// details of a each car instance 
		}
	}
}
