using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for AnotherCar.
	/// </summary>
	public class AnotherCar
	{
			private string colour;
			private string type;
			private string make;
			private string country;
			private double cost; 

			public AnotherCar() // constructor methods
			{
				country="UK"; 
			}
			public AnotherCar(string aMake)
			{
				country="UK";
				make= aMake;
			}
			public AnotherCar(string aMake, string aType)
			{
				country="UK";
				make= aMake; 	
				type= aType;
			}

			public string Country // read only Property
			{
				get { return country; }
			
			}
		
			public string Colour //property for manipulating colour
			{
				get {return colour; }
				set {colour=value; 	}
			}
			public string Type //property for manipulating type
			{
				get {return type;}
				set {type=value;}
			} 
		
			public string Make //property for manipulating make
			{
				get { return make; }
				set { make=value; }	
			}
			public double Cost //property for manipulating cost
			{
				get { return cost; }
				set { cost=value; }	
			}
            public void carDisplay() 
            {
                Console.WriteLine("The car details are" + " " + Make + " " + Type + " " + Colour + " " + Cost);

            }
    
    }
}
