using System;

namespace Classes5
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{

			// Put code here to create 3 car instances

			AnotherCar car1 = new AnotherCar();
            car1.Make="Honda";
            car1.Type ="Civic";
            car1.Colour ="Blue";
            car1.Cost=35400;
            Console.WriteLine("Car 1 :" + " " + car1.Type + " " + car1.Make + " " + car1.Colour + " " + car1.Cost);

            AnotherCar car2 = new AnotherCar();
            car2.Make = "Generic Car Name";
            car2.Type = "Generic Car Model";
            car2.Colour = "Green";
            car2.Cost = 354444;
            Console.WriteLine("Car 2 :" + " " + car2.Type + " " + car2.Make + " " + car2.Colour + " " + car2.Cost);

            AnotherCar car3 = new AnotherCar();
            car3.Make = "Ford";
            car3.Type = "Focus";
            car3.Colour = "Yellow";
            car3.Cost = 57464;
            Console.WriteLine("Car 3 :" + " " + car3.Type + " " + car3.Make + " " + car3.Colour + " " + car3.Cost);

            car1.carDisplay();
            car2.carDisplay();
            car3.carDisplay();


            Console.ReadLine();

		}
	}
}
